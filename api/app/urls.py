from django.urls import path
from .views import UserViews

urlpatterns=[
    path("users/",UserViews.as_view()),
    path("users/<uuid:id>", UserViews.as_view())
]
from functools import partial
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import UserSerializer
from .models import User

class UserViews(APIView):

    def post(self,request):
        serializer=UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            response={
                "status":"success",
                "data":serializer.data
            }
            return Response(response,status=status.HTTP_200_OK)
        else:
            response={
                "status":"failed",
                "data":serializer.errors
            }
            return Response(response,status=status.HTTP_400_BAD_REQUEST)

    def get(self,request,id=None):
        if id:
            user=User.objects.get(id=id)
            serializer=UserSerializer(user)

            response={
                "status":"success",
                "data":serializer.data
            }

            return Response(response,status=status.HTTP_200_OK)
        
        user=User.objects.all()
        serializer=UserSerializer(user, many=True)
        response={
            "status":"success",
            "data":serializer.data
        }
        return Response(response,status=status.HTTP_200_OK)

    def patch(self, request, id=None):
        user=User.objects.get(id=id)
        serializer=UserSerializer(user, data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            response={
                "status":"success",
                "data":serializer.data
            }

            return Response(response, status=status.HTTP_200_OK)
        else:
            response={
                "status":"failed",
                "data":serializer.errors
            }
            return Response(response, status=status.HTTP_400_BAD_REQUEST)



    def delete(self,request,id=None):
        user=get_object_or_404(User,id=id)
        user.delete()
        response={
            "status":"success",
            "data":"User deleted"
        }
        return Response(response,status=status.HTTP_200_OK)

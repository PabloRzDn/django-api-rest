from django.db import models
import uuid

class User(models.Model):
    id=models.UUIDField(primary_key=True,default=uuid.uuid4,editable=False)
    name=models.CharField(max_length=255)
    lastname=models.CharField(max_length=255)
    address=models.CharField(max_length=255)
    sign_date=models.DateField()

    def __str__(self):
        return self.name
from django.test import TestCase
from .factories import UserFactory


class UserTestCase(TestCase):
    def test_str(self):
        user=UserFactory()
        self.assertEqual(str(user),user.name)


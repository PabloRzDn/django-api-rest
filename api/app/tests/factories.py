from ..models import User
import factory


class UserFactory(factory.django.DjangoModelFactory):
    name=factory.Faker("first_name")
    lastname=factory.Faker("last_name")
    address=factory.Faker("address")
    sign_date=factory.Faker("date")

    class Meta:
        model=User

        